package server

import (
	"bitbucket.org/zaphome/sharedapi/logging"
	"bitbucket.org/zaphome/sharedapi/context"
	"net/http"
)

var (
	ctx *context.Context
	logger logging.Logger
)

func Initialize(c *context.Context) {
	ctx = c
	logger = (*ctx.GetService("Logger")).(logging.Logger)
	go serve()
}

func serve() {
	http.Handle("/", http.FileServer(http.Dir("webapp")))
	logger.ErrorOnError(http.ListenAndServe(":80", nil))
}
