package main

import (
	"bitbucket.org/zaphome/adapterbase/configuration"
	"bitbucket.org/zaphome/sharedapi/adapters"
	"bitbucket.org/zaphome/sharedapi/context"
	"bitbucket.org/zaphome/webadapter/server"
)

const AdapterName = "Web Front-end"

var ctx *context.Context

func Initialize(adapt adapters.Adapter) adapters.Adapter {
	adapt.Name = AdapterName

	ctx = configuration.Init(&adapt)
	server.Initialize(ctx)
	return adapt
}
